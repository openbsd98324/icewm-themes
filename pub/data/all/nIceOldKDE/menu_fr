prog "XTerm" xterm xterm
prog "XFTerminal" xterm xfce4-terminal 
prog "Thunar" folder-home thunar 
separator

menu "Accessoires" applications-accessories {
	menu "Archivage" folder {
		prog "Gestionnaire d’archives" org.gnome.ArchiveManager file-roller
		prog "Xarchiver" xarchiver xarchiver
	}

	prog "Capture d’écran" org.gnome.Screenshot gnome-screenshot --interactive
	prog "Antivirus ClamTk" clamtk clamtk
	prog "Disques" org.gnome.DiskUtility gnome-disks
	prog "Galculator" galculator galculator
	prog "Gnote" org.gnome.Gnote gnote
	prog "Table de caractères" accessories-character-map gucharmap
	
	menu "Editeurs de Texte" accessories-text-editor {
		prog "GVim" gvim icewm-menu-fdo "/usr/share/applications/gvim.desktop"
		prog "Mousepad" org.xfce.mousepad mousepad
	}
}


menu "Bureautique" applications-office {
	prog "LibreOffice" libreoffice-startcenter libreoffice
	prog "LibreOffice Writer" libreoffice-writer icewm-menu-fdo "/usr/share/applications/libreoffice-writer.desktop"
	prog "LibreOffice Calc" libreoffice-calc icewm-menu-fdo "/usr/share/applications/libreoffice-calc.desktop"
	prog "LibreOffice Impress" libreoffice-impress icewm-menu-fdo "/usr/share/applications/libreoffice-impress.desktop"
	prog "LibreOffice Math" libreoffice-math icewm-menu-fdo "/usr/share/applications/libreoffice-math.desktop"
	prog "LibreOffice Draw" libreoffice-draw icewm-menu-fdo "/usr/share/applications/libreoffice-draw.desktop"
	prog "LibreOffice Base" libreoffice-base icewm-menu-fdo "/usr/share/applications/libreoffice-base.desktop"
	prog "GnuCash" gnucash-icon gnucash
	prog "Visionneur de PDF" mupdf mupdf-gl
	prog "Visionneur de documents" org.gnome.Evince evince
}


menu "Graphisme" applications-graphics {
	prog "Éditeur d’image GIMP" gimp gimp-2.10
	prog "Visionneur Gthumb"   gthumb gthumb
	prog "Visionneur Gpicview" gpicview gpicview
	prog "ImageMagick" display-im6.q16 icewm-menu-fdo "/usr/share/applications/display-im6.q16.desktop"
	prog "Shotwell, gestion de photos" shotwell shotwell
	prog "OpenSCAD" openscad openscad
	prog "Inkscape" org.inkscape.Inkscape inkscape
	prog "Numériseur de documents" org.gnome.SimpleScan simple-scan
}


menu "Multimédia" applications-multimedia {
        prog "Controleur de volume PulseAudio" multimedia-volume-control pavucontrol
        prog "Graveur de CD Brasero" brasero brasero

        menu "Lecteurs" folder {
                prog "Audacious" audacious audacious
                prog "Multimedia Player mpv" mpv icewm-menu-fdo "/usr/share/applications/mpv.desktop"
                prog "LXMusic simple music player" lxmusic lxmusic
                prog "Rhythmbox Music Player" org.gnome.Rhythmbox3 rhythmbox
                prog "SMPlayer" smplayer smplayer
        }

        menu "Audio" applications-multimedia {
                prog "Audacity" audacity icewm-menu-fdo "/usr/share/applications/audacity.desktop"
        }

        prog "Sound Juicer" org.gnome.SoundJuicer sound-juicer
}


menu "Réseau et internet" applications-internet {
	prog "Navigateur Web Firefox ESR" firefox-esr /usr/lib/firefox-esr/firefox-esr
	prog "Messagerie Thunderbird" thunderbird /usr/bin/thunderbird
	prog "HexChat" io.github.Hexchat icewm-menu-fdo "/usr/share/applications/io.github.Hexchat.desktop"
	prog "Deluge BitTorrent Client" deluge deluge-gtk
	prog "Microsoft Teams - Preview" teams teams
	prog "Remmina Remote Desktop Client" org.remmina.Remmina remmina-file-wrapper
	prog "Visionneur de bureaux distants" preferences-desktop-remote-desktop vinagre
	prog "Zoom" Zoom /usr/bin/zoom
}


menu "Programmation" applications-development {
	prog "IDLE" /usr/share/pixmaps/idle.xpm /usr/bin/idle
	prog "IDLE (using Python-3.10)" /usr/share/pixmaps/python3.10.xpm /usr/bin/idle-python3.10
	prog "Jupyter Notebook" notebook x-terminal-emulator -e jupyter-notebook
	prog "RStudio" rstudio /usr/lib/rstudio/bin/rstudio
}


menu "Education/Sciences" applications-science {
	menu "Maths" folder {
		prog "GNU PSPP" org.gnu.pspp psppire
		prog "R" rlogo_icon x-terminal-emulator -e R
		prog "Xcas" xcas xcas
}
}


menu "Jeux" applications-games {
	prog "0 A.D." 0ad 0ad
	prog "Dungeon Crawl" crawl x-terminal-emulator -e /usr/games/crawl
	prog "Dungeon Crawl (tiles)" crawl /usr/games/crawl-tiles
	prog "X NetHack" nethack /usr/games/xnethack
	prog "SuperTux 2" supertux2 supertux2
	prog "SuperTuxKart" supertuxkart supertuxkart
	prog "Quadrapassel" org.gnome.Quadrapassel quadrapassel
	prog "Reversi" org.gnome.Reversi iagno
	prog "Solitaire AisleRiot" gnome-aisleriot sol
	prog "Tali" org.gnome.Tali tali
	prog "Cinq ou plus" org.gnome.five-or-more five-or-more
	prog "Hitori" org.gnome.Hitori hitori
	prog "Quatre-à-la-suite" org.gnome.Four-in-a-row four-in-a-row
	prog "Swell-Foop" org.gnome.SwellFoop swell-foop
	prog "Tout-éteint" org.gnome.LightsOff lightsoff
}



menu "Préférences" preferences-other {
	prog "Adobe Flash Player" flash-player-properties flash-player-properties
	prog "Contrôle du volume PulseAudio" multimedia-volume-control pavucontrol
	prog "Date et heure" time-admin time-admin

	menu "Préférences du bureau" preferences-desktop {
		prog "Configuration des raccourcis clavier" preferences-desktop-keyboard lxhotkey --gui=gtk
		prog "Économiseur d'écran"    xscreensaver xscreensaver-settings
		prog "Paramètres de Mousepad" org.xfce.mousepad mousepad --preferences
		prog "Apparence du bureau"    preferences-desktop-theme lxappearance
		prog "Préférences du bureau"  user-desktop pcmanfm --desktop-pref
	}


	menu "Préférences du matériel" preferences-desktop-peripherals {
		prog "Clavier et souris" input-keyboard lxinput
		prog "Connman Settings" preferences-system-network connman-gtk
		prog "Gestionnaire Bluetooth" blueman blueman-manager
		prog "Paramètres de l'écran" video-display lxrandr
	}

	prog "Méthode d'entrée" input-keyboard im-config
	prog "OpenJDK Java 8 - réglage" openjdk-8 /usr/bin/policytool
	prog "Gestionnaire de paquets Synaptic" synaptic synaptic-pkexec
	prog "Préférences de IBus" ibus-setup ibus-setup
	prog "Renseignements personnels" user_icon userinfo
	prog "Utilisateurs et groupes" config-users users-admin
	prog "TeXdoctk" - texdoctk
}


menu "Système" preferences-system {
	prog "Date et heure" time-admin time-admin

	menu "Gestionnaires de fichiers" system-file-manager {
		prog "PCManFM" system-file-manager pcmanfm
		prog "Midnight Commander" MidnightCommander x-terminal-emulator -e mc
	}

	menu "Gestion des paquets" system-software-install {
		prog "Gestionnaire de paquets Synaptic" synaptic synaptic-pkexec
		prog "Installateur de paquets GDebi" gnome-mime-application-x-deb gdebi-gtk
	}


	menu "Moniteurs système"  utilities-system-monitor {
		prog "XLoad" xload xload
		prog "conky" conky-logomark-violet conky --daemonize --pause=1
		prog "Gestionnaire des tâches" utilities-system-monitor lxtask
	}

	menu "Terminaux" utilities-terminal {
		prog "UXTerm" mini.xterm uxterm
		prog "LXTerminal" lxterminal lxterminal
		prog "XTerm" mini.xterm xterm
	}

	prog "Utilisateurs et groupes" config-users users-admin
}

separator

prog "XKill" xkill_32x32.xpm xkill


separator

menu "Autres" applications-other {
	prog "Commodore 64" /usr/share/pixmaps/c64icon-32x28.xpm /usr/bin/x64sc
}
